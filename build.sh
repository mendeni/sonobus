#!/bin/bash +xe

export DEBIAN_FRONTEND=noninteractive

apt-get update

apt-get -y install build-essential git pkg-config libfreetype-dev libasound2-dev libcurl4-openssl-dev libopus-dev libjack-dev libxrandr-dev libxinerama-dev libxcursor-dev mesa-common-dev cmake

git clone --branch $1 https://github.com/essej/sonobus.git sonobus

cd sonobus

./setupcmake.sh
./buildcmake.sh
