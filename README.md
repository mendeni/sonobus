# Usage
./build.sh - Installs package building deps, clones [upstream source](https://github.com/essej/sonobus), builds SonoBus binary.

# Miscellaneous
* Built and tested on Ubuntu 20.04, your milage may vary on other OS's.
